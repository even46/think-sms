<?php

namespace frappe\sms;

use InvalidArgumentException;
use think\Config;
use frappe\sms\aliyun\AliyunSms;
use frappe\sms\exceptions\SmsException;

class Sms
{
    /**
     * Thinkphp Config
     * @var Config|null
     */
    protected $config = null;

    /**
     * 短信服务商
     * @var string|null
     */
    private $service = null;

    /**
     * 短信服务商配置
     * @var array
     */
    private $options = [];

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * 服务商配置
     * @param string|null $config
     * @author yinxu
     * @date 2024/3/22 19:28:53
     */
    protected function configure(string $config = null): void
    {
        if (is_null($config)) {
            $config = $this->config->get('sms.default', '');
        }

        if (empty($config)) {
            throw new InvalidArgumentException("Miss Sms Config Name");
        }

        $services = $this->config->get('sms.services', []);

        if (empty($services) || !isset($services[$config]) || !is_array($services[$config])) {
            throw new InvalidArgumentException("Miss Sms $config Config");
        }

        $this->service = $config;
        $this->options = $services[$config];
    }

    /**
     * 发送短信
     * @param string|array $phones 手机号
     * @param string $code 模版CODE
     * @param array $params 短信参数
     * @param string|null $config
     * @param array $extra 扩展数据：outId｜smsUpExtendCode
     * @return bool
     * @throws SmsException
     * @author yinxu
     * @date 2024/3/22 19:33:03
     */
    public function send($phones, string $code, array $params = [], string $config = null, array $extra = []): bool
    {
        $this->configure($config);

        if ($this->service == "aliyun") {
            $sms = new AliyunSms($this->options);
            $outId = $extra['outId'] ?? "";
            $smsUpExtendCode = $extra['smsUpExtendCode'] ?? "";
            return $sms->send($phones, $code, $params, $outId, $smsUpExtendCode);
        }

        throw new InvalidArgumentException("Miss Sms Service $this->service Unadapted.");
    }
}