<?php
// +----------------------------------------------------------------------
// | Sms配置文件
// +----------------------------------------------------------------------

return [
    // 默认阿里云短信
    'default' => 'aliyun',
    // 短信服务商列表
    'services' => [
        // 阿里云短信
        'aliyun' => [
            'accessKeyId'       => '',
            'accessKeySecret'   => '',
            'signName'          => '',
            'domain'            => 'dysmsapi.aliyuncs.com',
            'regionId'          => 'cn-hangzhou',
            'version'           => '2017-05-25',
            'template'          => [
                'login'                 => '',
                'register'              => '',
                'forgot'                => '',
            ],
        ],
    ],
    'limit' => [
        // 间隔60s发送一次
        'second' => 60,
        // 24小时仅能发送10条
        'day' => 10,
    ]
];