<?php

namespace frappe\sms\facade;

use think\Facade;

/**
 * Class Sms
 * @package frappe\sms\facade
 * @mixin \frappe\sms\Sms
 */
class Sms extends Facade
{
    protected static function getFacadeClass()
    {
        return \frappe\sms\Sms::class;
    }
}
